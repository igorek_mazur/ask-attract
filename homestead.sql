-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 15, 2015 at 04:09 AM
-- Server version: 5.5.41-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `homestead`
--

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `countries_name_unique` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=17 ;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Ukraine', '2015-06-14 17:57:34', '2015-06-14 17:57:34'),
(2, 'Afghanistan', '2015-06-14 17:57:34', '2015-06-14 17:57:34'),
(3, 'Albania', '2015-06-14 17:57:34', '2015-06-14 17:57:34'),
(4, 'Andorra', '2015-06-14 17:57:34', '2015-06-14 17:57:34'),
(5, 'Angola', '2015-06-14 17:57:34', '2015-06-14 17:57:34'),
(6, 'Argentina', '2015-06-14 17:57:34', '2015-06-14 17:57:34'),
(7, 'Armenia', '2015-06-14 17:57:34', '2015-06-14 17:57:34'),
(8, 'Australia', '2015-06-14 17:57:34', '2015-06-14 17:57:34'),
(9, 'Bahamas', '2015-06-14 17:57:34', '2015-06-14 17:57:34'),
(10, 'Brazil', '2015-06-14 17:57:34', '2015-06-14 17:57:34'),
(11, 'Canada', '2015-06-14 17:57:34', '2015-06-14 17:57:34'),
(12, 'Chad', '2015-06-14 17:57:34', '2015-06-14 17:57:34'),
(13, 'China', '2015-06-14 17:57:34', '2015-06-14 17:57:34'),
(14, 'Colombia', '2015-06-14 17:57:34', '2015-06-14 17:57:34'),
(15, 'Costa Rica', '2015-06-14 17:57:34', '2015-06-14 17:57:34'),
(16, 'Cuba', '2015-06-14 17:57:34', '2015-06-14 17:57:34');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2015_06_13_171941_create_countries_table', 1),
('2015_06_13_171949_create_questions_table', 1),
('2015_06_13_172025_modify_users_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE IF NOT EXISTS `questions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `country_id` int(10) unsigned NOT NULL,
  `views_count` int(10) unsigned NOT NULL,
  `approved` tinyint(1) NOT NULL DEFAULT '0',
  `is_answered` tinyint(1) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `answer` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `questions_country_id_foreign` (`country_id`),
  KEY `questions_user_id_foreign` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `user_id`, `country_id`, `views_count`, `approved`, `is_answered`, `title`, `body`, `answer`, `created_at`, `updated_at`) VALUES
(1, 2, 1, 1, 1, 0, '10-Quisque vel ullamcorper mauris. In sed efficitur ligula', 'Nunc cursus libero sem, vel vulputate ante maximus ac. Ut mi quam, feugiat pretium ultrices ut, eleifend ac nunc.', 'Nunc cursus libero sem, vel vulputate ante maximus ac. Ut mi quam, feugiat pretium ultrices ut, eleifend ac nunc. Vivamus dignissim gravida vehicula. Duis mattis sem est, non mattis nisi vestibulum ut. Proin a enim eu lacus congue sodales vel nec quam. Aliquam et finibus lectus, eget maximus purus. Vestibulum ac varius libero, a tincidunt felis. Duis cursus tristique varius.\r\n', '2015-06-14 18:10:48', '2015-06-14 20:31:31'),
(2, 2, 1, 1, 1, 0, '9-Donec elit ante, mattis id bibendum eget, egestas id lorem.', 'Donec elit ante, mattis id bibendum eget, egestas id lorem. Nulla eu magna aliquam, imperdiet ipsum in, molestie mauris. Quisque eu sem non dolor fermentum fringilla sit amet id libero. Donec tincidunt diam sem, sed scelerisque sem tempor quis. Sed auctor erat nec turpis posuere, nec venenatis ligula facilisis. Suspendisse interdum ligula ac eros vestibulum, vel eleifend felis rutrum.', 'Proin ut bibendum tellus, nec condimentum sem. In mi massa, bibendum in lectus eget, tristique vestibulum neque. Integer a velit quis metus rhoncus lacinia. Cras pulvinar orci eget vestibulum bibendum. Etiam at nulla a neque facilisis semper at eget quam. Ut id aliquet nisl. ', '2015-06-14 18:20:23', '2015-06-14 21:41:57'),
(3, 2, 1, 0, 1, 0, '8-Ut vitae justo neque. ', 'Ut vitae justo neque. Quisque maximus est sed nisl tempus tincidunt. Pellentesque suscipit, ante vitae semper placerat, massa lorem imperdiet erat, ut consectetur magna nisl non ante. Ut pharetra quis nisi et gravida. Pellentesque rhoncus bibendum tellus, a convallis arcu faucibus eget. Nunc faucibus tortor risus, sit amet blandit diam ornare et. Maecenas commodo quam in interdum tincidunt. Duis nec sollicitudin massa. Suspendisse vel nisl felis. Curabitur libero metus, lacinia quis egestas ac, aliquam at nunc.', ' Nam semper, turpis et aliquam sagittis, mi dolor laoreet ante, sit amet aliquam sem nulla at enim. Nunc ante dui, mollis in sapien non, finibus volutpat eros. Phasellus tincidunt, turpis aliquam tristique scelerisque, nibh nisl posuere odio, id consequat erat mi et mauris.', '2015-06-14 18:20:52', '2015-06-14 20:31:12'),
(4, 2, 1, 0, 1, 0, '7-Aliquam blandit, lacus id tempus pretium, leo erat congue augue.', 'Aliquam blandit, lacus id tempus pretium, leo erat congue augue, sed mollis tellus nibh vel urna. Maecenas sit amet quam libero. Nunc consequat ultrices neque, ac condimentum lorem laoreet ac. Praesent ipsum tortor, dictum viverra risus vitae, faucibus facilisis mauris. In sollicitudin non augue nec aliquet. Pellentesque ac arcu vitae eros sagittis congue. Phasellus molestie fermentum risus eu lacinia. Suspendisse rhoncus in elit id tristique.', ' Donec placerat mollis mauris, ut aliquam ex rutrum vel. Vestibulum ultricies et ante a tempor. Integer dignissim aliquet augue ac mattis. Vivamus quis velit auctor, efficitur ante eu, porttitor urna. Quisque tempor purus non suscipit lobortis.', '2015-06-14 18:21:24', '2015-06-14 21:28:30'),
(5, 2, 1, 10, 1, 0, '6-Fusce cursus nisl vitae lectus efficitur, non mattis dolor dignissim. ', 'Fusce cursus nisl vitae lectus efficitur, non mattis dolor dignissim. Nulla porttitor, quam sit amet ultricies porttitor, eros lacus facilisis ligula, nec vestibulum orci sem vitae mi. Aliquam pellentesque purus posuere turpis dapibus pellentesque. Vivamus at libero justo. Vivamus nulla ligula, rutrum sit amet elementum ut, porttitor eget nisi. Nulla porttitor felis nec interdum auctor. Integer nibh tortor, tincidunt in maximus et, porta nec dolor. Duis massa elit, eleifend eu auctor eu, mattis ac est.\r\n', ' Nunc ultrices hendrerit purus quis dignissim. Suspendisse luctus finibus urna, a efficitur nibh. Nulla rutrum urna diam, non posuere sapien lacinia in. Fusce pellentesque ex ut odio dapibus pulvinar. Maecenas venenatis faucibus leo vel feugiat. Nulla faucibus at massa quis vehicula.', '2015-06-14 18:22:22', '2015-06-14 21:41:47'),
(6, 2, 1, 0, 1, 0, '5-Quisque ut mi non dolor gravida congue et a justo. ', 'Quisque ut mi non dolor gravida congue et a justo. Donec leo lorem, ullamcorper eget vestibulum eu, scelerisque sit amet libero. Quisque id mi augue. Sed vestibulum, sem eu commodo bibendum, ipsum sapien tincidunt metus, non scelerisque neque lacus ac orci. Morbi maximus enim a sem placerat, a faucibus purus laoreet. Pellentesque quis magna lorem. Aenean imperdiet sit amet sem vitae hendrerit. Nunc velit enim, laoreet id lacus vitae, posuere pharetra enim. Duis fringilla erat mauris, a dapibus leo ullamcorper sed. Donec quis tortor id leo bibendum porta. Quisque imperdiet, enim a venenatis condimentum, sem arcu cursus dui, nec iaculis sem risus id elit. Aenean volutpat, neque nec suscipit suscipit, erat massa malesuada ante, id convallis nibh nunc sit amet arcu. Ut consequat vestibulum enim at rutrum.', ' Nulla vehicula, turpis sit amet lobortis mattis, magna orci scelerisque neque, id ullamcorper mauris augue sagittis nunc. In vulputate, sapien eget pellentesque tristique, tellus odio luctus leo, at venenatis sapien nisl mollis turpis.', '2015-06-14 18:23:15', '2015-06-14 20:30:45'),
(7, 2, 1, 0, 1, 0, '4-Nam id neque sed leo rhoncus pretium dictum nec metus.', 'Nam id neque sed leo rhoncus pretium dictum nec metus. Fusce tristique purus dolor, consequat condimentum ante dignissim vel. Mauris vulputate finibus ante. Morbi sed ante at ligula vestibulum bibendum. Nulla viverra magna sit amet enim pulvinar, sed dignissim lorem efficitur.', 'Cras volutpat metus vel viverra tristique. Quisque feugiat nisl erat, ac condimentum tellus maximus ac. Donec quis mi sit amet urna mollis varius vel nec leo. Sed egestas nisi enim, eu consequat diam pharetra quis. Nunc imperdiet auctor ullamcorper. Phasellus feugiat nisl vel augue eleifend placerat. Curabitur id enim erat. Quisque in tristique dolor, et ultrices mi. Etiam tempus vel elit sed tincidunt.', '2015-06-14 18:24:45', '2015-06-14 20:30:37'),
(8, 2, 1, 3, 1, 0, '3-Donec malesuada lacinia vehicula.', 'Donec malesuada lacinia vehicula. Nullam bibendum vulputate eros a viverra. Vestibulum condimentum semper est, sed lobortis justo sagittis vitae. Curabitur mollis ultricies nunc non volutpat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla facilisi. Vivamus semper velit porttitor lacus rutrum feugiat.', ' Maecenas mauris ipsum, consectetur eget lacus eu, blandit euismod urna. Integer vitae lacinia magna, ut tempus urna. Phasellus at condimentum augue. Fusce suscipit condimentum arcu, ut condimentum justo aliquam in. Integer sit amet ullamcorper ipsum. Integer in ex urna. Mauris quis euismod leo.', '2015-06-14 18:25:04', '2015-06-14 21:54:51'),
(9, 2, 1, 0, 0, 0, '2-Fusce vehicula augue vitae est aliquet, id ultrices velit convallis.', 'Fusce vehicula augue vitae est aliquet, id ultrices velit convallis. Vestibulum fringilla aliquam suscipit. Nam id nulla sed dolor commodo congue. Nullam volutpat at nunc sed auctor. Etiam lorem orci, sodales ut sagittis at, lacinia non nulla. ', 'Vestibulum tempor eros vel est faucibus ultrices. Vivamus a elit varius, laoreet odio eget, iaculis leo. Sed convallis ligula in orci rutrum, sit amet tincidunt risus vestibulum. Phasellus commodo ultricies urna, sed interdum orci ullamcorper vel.', '2015-06-14 18:25:23', '2015-06-14 21:06:30'),
(10, 2, 1, 0, 1, 0, '1-Pellentesque est orci, imperdiet ut luctus at, tempor ac mi. ', 'Pellentesque est orci, imperdiet ut luctus at, tempor ac mi. Sed fringilla ligula non turpis tempus dapibus. Praesent sit amet justo at orci consequat bibendum vitae ac quam. Fusce non eleifend elit. Integer in felis quis risus pharetra porttitor. Maecenas lorem lectus, condimentum et tempor sed, vehicula a orci.\r\n', ' Vestibulum eget aliquam ipsum. Sed arcu justo, dictum ut quam id, dapibus tincidunt ex. Donec feugiat rutrum tellus vitae congue. Nullam bibendum porttitor bibendum. Duis ex quam, sagittis in fermentum consectetur, tincidunt vitae massa.', '2015-06-14 18:25:52', '2015-06-14 21:06:26'),
(11, 3, 10, 0, 1, 0, ' Aenean non mattis nulla.', 'Vivamus ornare rutrum dolor. Morbi hendrerit, tortor sed aliquam accumsan, dolor diam malesuada neque, sit amet bibendum odio urna quis metus. Vivamus dictum sem nec nunc gravida sagittis. Aenean non mattis nulla.', 'Aenean non mattis nulla.Morbi hendrerit, tortor sed aliquam accumsan, dolor diam malesuada neque.', '2015-06-14 21:39:40', '2015-06-14 22:02:45'),
(12, 1, 1, 0, 0, 0, 'Fusce id dignissim sapien, ut viverra ipsum.', '  Fusce at condimentum lectus. Cras eu tempus neque, eget malesuada tellus. Ut tincidunt mauris et velit placerat commodo. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris sed justo sit amet diam ultrices sodales.', '', '2015-06-14 21:41:28', '2015-06-14 21:41:28');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `country_id` int(10) unsigned NOT NULL,
  `is_superuser` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_country_id_foreign` (`country_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `country_id`, `is_superuser`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Igor Mazur', 'igorek_mazur@mail.ru', '$2y$10$ds8lvfIQjjIsVgJ5oWiBveoX32oXl1h6fmv1BTD.EWtq57WpjvAXu', 'sRVI5faEmDaQreMXUMdHhtIALAVOKIctinjL4Il8YikBS1Blb6OYfkBa2v8g', '2015-06-14 17:57:34', '2015-06-14 22:01:41'),
(2, 1, 1, 'admin@localhost.com', 'admin@localhost.com', '$2y$10$tpWk2ZRHXYEFH5SMcWpLIuz7JUtP376NI8GzqSXHEpxB/rjf5DVq2', 'banoK3tw3vqUZgpDhEl2mI6Nehl4RrtB7iYdv2WCmeMMJyqJbNQtxa81cimw', '2015-06-14 17:57:34', '2015-06-14 21:37:03'),
(3, 10, 0, 'Test User', 'test_user@gmail.com', '$2y$10$FcuDfKn9mCrpIjj2YvKHJecE8i3jyzvJpsuOckXRpKnDTQ1nUlqua', 'A7rV1ioDgTRctRHTRdaae1ElPoIGO8qe2Of1YAEpm0LujGEEXijlajPp8Egz', '2015-06-14 21:04:16', '2015-06-14 21:40:08'),
(4, 7, 0, 'Test Rgister User', 'tru@localhost.com', '$2y$10$Qs0Ci2RU2BzVqdy9RWzgCO0kCDIifEWbYpYGqh.La/U2ilylTFNra', 'tkP8JkKZE3nJwcxgf5JfjkkPpLNbitBW823rSCZAUXkxPPgi3n5IZMssWr60', '2015-06-14 21:34:40', '2015-06-14 21:36:20');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `questions`
--
ALTER TABLE `questions`
  ADD CONSTRAINT `questions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `questions_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
