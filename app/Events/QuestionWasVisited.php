<?php

namespace App\Events;

use App\Events\Event;
use App\Models\Question;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class QuestionWasVisited extends Event
{
    use SerializesModels;

    public $questionId;

    /**
     * Create a new event instance.
     *
     * @param $questionId
     */
    public function __construct($questionId)
    {
        $this->questionId = $questionId;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
