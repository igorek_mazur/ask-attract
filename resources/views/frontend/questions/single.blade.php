@extends('app')

@section('top-scripts')

@stop

@section('content')
    <div class="col-xs-12">
        <p class="pull-right">
            <span class="label label-{{ $question->approved ? 'success' : 'default' }}">Approved</span>
        </p>
        <blockquote>
            <h4>{{$question->title}}</h4>
            <p>{{ $question->body }}</p>
        </blockquote>
         <blockquote class="blockquote-reverse">
            {{ $question->answer }}
        </blockquote>
    </div>
@stop

@section('bottom-scripts')

@stop
