@foreach($questions as $key=>$question)
    <div class="col-xs-12 {{ $key % 2 !== 0 ? 'highlight' : null }}">
        <p class="pull-right">
            <span class="label label-success">Approved</span>
        </p>
        <h4>{{$question->title}}</h4>
        <p>{{ $question->body }}</p>
        <p><a class="btn btn-default" href="{!! route('frontend.get.single',$question->id) !!}" role="button">View details »</a></p>
    </div>
@endforeach