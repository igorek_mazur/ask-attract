@extends('app')

@section('top-scripts')

@stop

@section('content')
    <div class="col-xs-12">
        {!! Form::open(['route'=>'frontend.post.asc','method'=>'post']) !!}
        <p class="caption-curd">Ask an Expert</p>
        <hr/>
        <div class="row">
            @include('partials.errors')
        </div>
        <div class="row">
            <div class="col-sm-10">
                <div class="form-group">
                    {!! Form::label('title','Subject') !!}
                    {!! Form::text('title',$value = null, ['class'=>'form-control','placeholder'=>'Enter Subject']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('body','Question') !!}
                    {!! Form::textarea('body',$value = null, ['rows'=>10,'class'=>'form-control','placeholder'=>'Enter Question Body']) !!}
                </div>
                <div class="form-group">
                    <p>{!!  captcha_img() !!}</p>
                    <input name="captcha" type="text" value="" class="form-control"/>
                </div>
                <div class="form-group">
                    {!! Form::submit('Send',['class'=>'form-control']) !!}
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('bottom-scripts')

@stop
