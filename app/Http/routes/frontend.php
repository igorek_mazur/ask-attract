<?php
	Route::group(['prefix'=>'', 'middleware'=>['auth'],'namespace'=>'Frontend'], function() {

		Route::get('/',['as'=>'frontend.get.seeAll','uses'=>'AskController@seeAll']);

		Route::get('ask',['as'=>'frontend.get.asc','uses'=>'AskController@getQuestion']);
		Route::post('ask',['as'=>'frontend.post.asc','uses'=>'AskController@postSaveQuestion']);

		Route::get('single/{id}',['as'=>'frontend.get.single','uses'=>'AskController@singleQuestion']);
		Route::get('my-questions',['as'=>'frontend.get.my-questions','uses'=>'AskController@getMyQuestions']);
	});
