<?php

namespace App\Http\Requests\Question;

use App\Http\Requests\Request;

class CreateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:4',
            'body'  => 'required|min:10',
            'captcha' => 'required|captcha',
        ];
    }

    public function messages()
    {
        return [
            'captcha'=>'Captcha does not match'
        ];
    }
}
