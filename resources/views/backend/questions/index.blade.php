@extends('app')

@section('top-scripts')

@stop

@section('content')
    <div class="col-xs-12">
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th></th>
                    <th>Question</th>
                    <th>Is Approved</th>
                    <th colspan="2">Options</th>
                </tr>
                </thead>
                <tbody>
                @forelse($questions as $key=>$question)
                    <tr>
                        <th scope="row">{{ $key+1 }} </th>
                        <td><a href="{!! route('backend.questions.edit',$question->id) !!}">{{$question->title}}</a></td>
                        <td>
                            @if($question->approved)
                                <span class="label label-success">yes</span>
                            @else
                                <span class="label label-default">no</span>
                            @endif
                       </td>
                        <td><a href="{!! route('backend.questions.edit',$question->id) !!}"><i class="fa fa-pencil-square-o"> Edit</i></a></td>
                        <td><a href="{!! route('backend.questions.getDelete',$question->id) !!}"><i class="fa fa-times"> Delete</i></a></td>
                    </tr>
                @empty
                <tr>
                    <td colspan="5">
                        <h5 class="text-center ">No Data Available</h5>
                    </td>
                </tr>      
                @endforelse
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('bottom-scripts')

@stop
