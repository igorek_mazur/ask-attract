<?php
namespace App\Handlers\Events;

use App\Models\Visitor;
use Auth;
use App\Events\QuestionWasVisited;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class IncrementVisitsCount
{
    /**
     * Create the event handler.
     *
     * @param Visitor $visitor
     */
    public function __construct(Visitor $visitor)
    {
        $this->visitor = $visitor;
    }

    /**
     * Handle the event.
     *
     * @param  QuestionWasVisited  $event
     * @return void
     */
    public function handle(QuestionWasVisited $event)
    {
        $auhUser = Auth::user();

        $visitorArray = ['country_id'=>$auhUser->country_id, 'question_id'=>$event->questionId, 'user_id'=>$auhUser->id];

        $this->visitor->firstOrCreate($visitorArray);

    }

}
