<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests;
use App\Http\Requests\Question\UpdateRequest;
use App\Models\Question;

class QuestionsController extends BackendController
{
    public function __construct(Question $question)
    {
        $this->model = $question;
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('backend.questions.index')->withQuestions($this->model->with('user')->orderBy('id','desc')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return redirect()->route('backend.questions.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        return redirect()->route('backend.questions.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return redirect()->route('backend.questions.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        return view('backend.questions.edit',[$id])->with('user')->withQuestion($this->model->findOrFail($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param  int $id
     * @return Response
     */
    public function update(UpdateRequest $request, $id)
    {
        $this->model->findOrFail($id)->update($request->all());

        return redirect()->route('backend.questions.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->model->findOrFail($id)->delete();

        return redirect()->route('backend.questions.index');
    }

}
