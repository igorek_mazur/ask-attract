<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Country\CreateRequest;
use App\Http\Requests\Country\UpdateRequest;
use App\Models\Country;
use Illuminate\Http\Request;
use App\Http\Requests;

class CountriesController extends BackendController
{
    public function __construct(Country $model)
    {
        $this->model = $model;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('backend.countries.index')->withCountries($this->model->orderBy('name')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.countries.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateRequest $request
     * @return Response
     */
    public function store(CreateRequest $request)
    {
        $country = $this->model->create($request->all());

        return redirect()->route('backend.countries.edit',[$country->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return redirect()->route('backend.countries.index');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('backend.countries.edit')->withCountry($this->model->findOrFail($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param  int $id
     * @return Response
     */
    public function update(UpdateRequest $request, $id)
    {
        $this->model->findOrFail($id)->update($request->all());

        return redirect()->route('backend.countries.edit',$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->model->findOrFail($id)->delete();

        return redirect()->route('backend.countries.index');
    }

}
