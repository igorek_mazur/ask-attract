<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use BadMethodCallException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

abstract class BackendController extends Controller
{
    protected $model;

	/**
     * @param $id
     * @return mixed
     */
    public function getDelete($id)
    {
        if(method_exists($this,'destroy')) {
            return $this->destroy($id);
        }
        throw new BadMethodCallException;
    }

}
