@extends('app')

@section('top-scripts')

@stop

@section('content')
    <div class="col-xs-12">
        {!! Form::model($user,['route'=>['backend.users.update',$user->id],'method'=>'put']) !!}
            <p class="caption-curd">Update User</p>
            <hr/>
            <div class="row">
                @include('partials.errors')
            </div>
            <div class="row">
                <div class="col-sm-8">
                    <div class="form-group">
                        {!! Form::label('name','Name') !!}
                        {!! Form::text('name',$value = null, ['class'=>'form-control','placeholder'=>'Enter User Name']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('email','Email') !!}
                        {!! Form::email('email',$value = null, ['class'=>'form-control','placeholder'=>'Enter User Email']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('password','Password') !!}
                        {!! Form::password('password',['class'=>'form-control','placeholder'=>'Enter User Password']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('password_confirmation','Password Confirmation') !!}
                        {!! Form::password('password_confirmation', ['class'=>'form-control','placeholder'=>'Confirm User Password']) !!}
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="from-group">
                        {!! Form::label('country_id', 'Country') !!}
                        {!! Form::select('country_id',$countries,$user->country_id,['class'=>'form-control']) !!}
                    </div>
                    <div class="from-group">
                        <br/>
                        {!! Form::label('','Is Superuser?')!!}<br/>
                        <div class="btn-group" data-toggle="buttons">
                            <label class="btn btn-default btn-custom {{ $user->is_superuser == 1 ? 'active': ''}}">
                                {!!Form::radio('is_superuser',$value = 1, $user->is_superuser == 1 ? true : false) !!}Yes</label>
                            <label class="btn btn-default btn-custom {{ $user->is_superuser == 0 ? 'active': ''}}">
                                {!!Form::radio('is_superuser',$value = 0, $user->is_superuser == 0 ? true : false)!!}No
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        {!! Form::submit('Save',['class'=>'from-control btn btn-default']) !!}
                    </div>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
@stop

@section('bottom-scripts')

@stop
