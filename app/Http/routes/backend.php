<?php
	Route::group(['prefix'=>'backend', 'middleware'=>['auth','permissions'],'namespace'=>'Backend'], function() {
		Route::resource('countries','CountriesController');
		Route::resource('users','UsersController');
		Route::resource('questions','QuestionsController');

		foreach(['countries'=>'CountriesController','users'=>'UsersController','questions'=>'QuestionsController'] as $key=>$resource) {
			Route::get($key.'/{id}/delete',['as'=>'backend.' .$key. '.getDelete', 'uses'=> $resource. '@getDelete']);
		}

		Route::pattern('id', '\d+');
	});
