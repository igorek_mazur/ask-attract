@if(count($errors->all()))
    <div class="col-xm-12">
        @foreach($errors->all() as $error)
            <div class="col-xs-4">
                <div class=" alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {!! $error !!}
                </div>
            </div>
        @endforeach
    </div>
@endif
