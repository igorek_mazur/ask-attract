<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id',false,true);
            $table->integer('country_id',false,true);
            $table->integer('views_count',false,true);

            $table->boolean('approved')->default(0);
            $table->boolean('is_answered')->defalult(0);

            $table->string('title');
            $table->text('body');
            $table->text('answer');

            $table->timestamps();

            $table->foreign('country_id')->references('id')->on('countries')->onUpdate('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('questions',function(Blueprint $table) {
            $table->dropForeign('questions_country_id_foreign');
            $table->dropForeign('questions_user_id_foreign');
        });

        Schema::drop('questions');
    }

}
