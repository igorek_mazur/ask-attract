@extends('app')

@section('top-scripts')

@stop

@section('content')
    <div class="col-xs-12">
        {!! Form::model($question,['route'=>['backend.questions.update',$question->id],'method'=>'put']) !!}
            <p class="caption-curd">Update Question</p>
            <hr/>
            <div class="row">
                @include('partials.errors')
            </div>
            <div class="row">
                <div class="col-sm-8">
                    <div class="form-group">
                        {!! Form::label('title','Subject Of Question') !!}
                        {!! Form::text('title',$value = null, ['class'=>'form-control','placeholder'=>'Enter Title Of Question']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('body','Body Of Question' ) !!}
                        {!! Form::textarea('body',$value = null, ['rows'=>5,'class'=>'form-control','placeholder'=>'Enter Body Of Question']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('answer','Answer Of Question' ) !!}
                        {!! Form::textarea('answer',$value = null, ['rows'=>5,'class'=>'form-control','placeholder'=>'Enter Answer']) !!}
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="from-group">
                        <label for="">User Name</label>
                        <input value="{{ $question->user->name  or ''}}" type="text" class="form-control" disabled/>
                    </div>
                    <div class="from-group">
                        <br>
                        <label for="">Is Approved?</label><br>
                        <div class="btn-group" data-toggle="buttons">
                            <label class="btn btn-default btn-custom {{ $question->approved == 1 ? 'active' : '' }}">
                                {!! Form::radio('approved',$value = 1, $question->approved == 1 ? true : false) !!} Yes
                            </label>
                            <label class="btn btn-default btn-custom {{ $question->approved == 0 ? 'active' : '' }}">
                                {!! Form::radio('approved',$value = 0, $question->approved == 0 ? true : false) !!} No
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <button type="submit" class="btn btn-default">Save</button>
                    </div>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
@stop

@section('bottom-scripts')

@stop
