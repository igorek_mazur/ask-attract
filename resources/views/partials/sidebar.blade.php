<!--Sidebar-->
<div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar" role="navigation">
    @if(Auth::user()->isSuperuser() and Request::is('backend*'))
        <div class="list-group">
            <a href="{!! route('backend.questions.index') !!}" class="list-group-item {{ Request::is('backend/questions*') ? 'active':  null }}">Questions</a>
            <a href="{!! route('backend.users.index') !!}" class="list-group-item {{ Request::is('backend/users*') ? 'active':  null }}">Users</a>
            <a href="{!! route('backend.countries.index') !!}" class="list-group-item {{ Request::is('backend/countries*') ? 'active':  null }}">Countries</a>
        </div>
    @else
        <div class="list-group">
            <a href="{!! '/' !!}" class="list-group-item {{ Request::is('/') ? 'active':  null }}">See All</a>
            <a href="{!! route('frontend.get.asc') !!}" class="list-group-item {{ Request::is('ask*') ? 'active':  null }}">Ask</a>
            <a href="{!! route('frontend.get.my-questions') !!}" class="list-group-item {{ Request::is('my-questions*') ? 'active':  null }}">My Questions</a>
            {{--
                <a href="{!! route('backend.questions.index') !!}" class="list-group-item {{ Request::is('backend/questions*') ? 'active':  null }}">Questions</a>
                <a href="{!! route('backend.users.index') !!}" class="list-group-item {{ Request::is('backend/users*') ? 'active':  null }}">Users</a>
                <a href="{!! route('backend.countries.index') !!}" class="list-group-item {{ Request::is('backend/countries*') ? 'active':  null }}">Countries</a>
            --}}
        </div>
    @endif
</div>
<!--/Sidebar-->