@extends('app')

@section('top-scripts')

@stop

@section('content')
    <div class="col-xs-12">
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th><a href="{!! route('backend.countries.create') !!}" class="btn  btn-xs" title="Add New Country"><i class="fa fa-2x fa-plus-circle"></i></a></th>
                    <th>Country</th>
                    <th colspan="2">Options</th>
                </tr>
                </thead>
                <tbody>
                @forelse($countries as $key=>$country)
                    <tr>
                        <th scope="row">{{ $key+1 }} </th>
                        <td><a href="{!! route('backend.countries.edit',$country->id) !!}">{{$country->name}}</a></td>
                        <td><a href="{!! route('backend.countries.edit',$country->id) !!}"><i class="fa fa-pencil-square-o"> Edit</i></a></td>
                        <td><a href="{!! route('backend.countries.getDelete',$country->id) !!}"><i class="fa fa-times"> Delete</i></a></td>
                        <td>{!! '' !!}</td>
                    </tr>
                @empty
                <tr>
                    <td colspan="3">
                        <h5 class="text-center ">No Data Available</h5>
                    </td>
                </tr>      
                @endforelse

                </tbody>
            </table>
        </div>
    </div>
@stop

@section('bottom-scripts')

@stop
