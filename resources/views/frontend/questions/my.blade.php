@extends('app')

@section('top-scripts')

@stop

@section('content')
    @foreach($questions as $key=>$question)
        {!! $key > 0 ? '<br>' : null !!}

        <div class="col-xs-12 {{ $key % 2 !== 0 ? 'highlight' : null }}">
                 <p class="pull-right">
                    <span class="label label-{{ $question->approved ? 'success' : 'default' }}">Approved</span>
                 </p>
                <blockquote>
                <h4>{{$question->title}}</h4>
                <p>{{ $question->body }}</p>
            </blockquote>
            @if($question->answer)
                <blockquote class="blockquote-reverse">
                   {{ $question->answer }}
                </blockquote>
            @endif
        </div>
    @endforeach
@stop

@section('bottom-scripts')

@stop
