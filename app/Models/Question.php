<?php

namespace App\Models;

class Question extends Eloquent
{
	protected $fillable = ['title','body','answer','user_id','country_id','views_count','approved','is_answered'];

	public function user()
	{
		return $this->belongsTo('App\Models\User');
	}

	public function country()
	{
		return $this->belongsTo('App\Models\Country');
	}

}
