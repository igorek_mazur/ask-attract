<?php

namespace App\Models;


class Country extends Eloquent
{
	protected $fillable = ['name'];

	public function users()
	{
		return $this->hasMany('App\Models\User');
	}

	public function countries()
	{
		return $this->hasMany('App\Models\County');
	}
}
