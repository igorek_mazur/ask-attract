@extends('app')

@section('top-scripts')

@stop

@section('content')
    @foreach($popular as $key=>$question)
        <div class="col-xs-12  $key % 2 !== 0 ? 'highlight' : null ">
             <p class="pull-right">
                <span class="label label-{{ $question->approved == 1 ? 'success' : 'default' }}">Approved</span>
                <span class="label label-danger"> Popular</span>
            </p>
            <h4>{{$question->title}}</h4>
            <p>{{ $question->body }}</p>
            <p><a class="btn btn-default" href="{!! route('frontend.get.single',$question->id) !!}" role="button">View details »</a></p>
        </div>
    @endforeach
    <br/>
    <br/>
    <div class="questions-container" id="questions-container">
        @foreach($questions as $key=>$question)
            <div class="col-xs-12 {{ $key % 2 !== 0 ? 'highlight' : null }}">
                <p class="pull-right">
                    <span class="label label-success">Approved</span>
                </p>
                <h4>{{$question->title}}</h4>
                <p>{{ $question->body }}</p>
                <p><a class="btn btn-default" href="{!! route('frontend.get.single',$question->id) !!}" role="button">View details »</a></p>
            </div>
        @endforeach
    </div>
    @if(count($questions))
        <div class="col-xs-12">
            <br/>
            <br/>
            <a href="#" id="load" class="btn btn-default btn-block">See More</a>
        </div>
    @endif
@stop

@section('bottom-scripts')
    <script>
        $(document).ready(function(){
            var page = 2,
                loadBtn =  $('#load');

            $(document).on('click', '#load', function (e) {
                e.preventDefault();
                $.ajax({
                    url : '/?page=' + page,
                    dataType: 'json'
                }).done(function (data) {
                    if(data.content) {
                        page++;
                        $("#questions-container").append(data.content);

                    } else {
                        loadBtn.fadeOut("slow");
                    }
                }).fail(function () {
                    alert('Questions could not be loaded.');
                });
            });
        });
    </script>
@stop
