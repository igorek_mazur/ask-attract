@extends('app')

@section('top-scripts')@stop

@section('content')
    <div class="col-xs-12">
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th><a href="{!! route('backend.users.create') !!}" class="btn  btn-xs" title="Add New User"><i class="fa fa-2x fa-plus-circle"></i></a></th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Is Superuser</th>
                        <th>Country</th>
                        <th colspan='2'>Options</th>
                    </tr>
                </thead>
                <tbody>
                @forelse($users as $key=>$user)
                    <tr>
                        <th scope="row">{{ $key+1 }} </th>
                        <td><a href="{!! route('backend.users.edit',$user->id) !!}">{{$user->name}}</a></td>
                        <td>{{$user->email}}</td>
                        <td>
                            <span class="label label-{{ $user->is_superuser ? 'success' : 'default' }}">{{ $user->is_superuser ? 'yes': 'no'}}</span>
                        </td>
                        <td>{{$user->country->name or ''}}</td>
                        <td><a href="{!! route('backend.users.edit',$user->id) !!}"><i class="fa fa-pencil-square-o"> Edit</i></a></td>
                        <td><a href="{!! route('backend.users.getDelete',$user->id) !!}"><i class="fa fa-times"> Delete</i></a></td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="5">
                            <h5 class="text-center ">No Data Available</h5>
                        </td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('bottom-scripts')@stop
