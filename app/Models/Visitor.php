<?php

namespace App\Models;

class Visitor extends Eloquent
{
	//	public $timestamps = false;

    protected $fillable = [
	    'country_id',
	    'question_id',
	    'user_id',
	    //'views_count',
    ];

}
