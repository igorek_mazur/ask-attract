<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Visitor;
use Auth;
use App\Http\Requests;
use App\Models\Question;
use App\Events\QuestionWasVisited;
use App\Http\Controllers\Controller;
use App\Http\Requests\Question\CreateRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use Request;
use Response;

class AskController extends Controller
{
    protected  $question;

    protected $visitor;

    public function __construct(Question $question, Visitor $visitor)
    {
        $this->question = $question;
        $this->visitor  = $visitor;
    }

    public function seeAll()
    {
        $popularQuestions = $this->getPopular();

        $questions = $this->question->where('approved',1)
            ->whereNotIn('id',is_object($popularQuestions) ? array_pluck($popularQuestions,'id') : [])
            ->orderBy('id','DESC')
            ->paginate(2);

        if(Request::ajax()){
            $view = view("frontend.ajax.questions")->withQuestions($questions)->render();

            return Response::json(['total'=>$questions->total(), 'content'=>$view]);
        }

        return view('frontend.questions.index')->withPopular($popularQuestions)->withQuestions($questions);
    }

    public function singleQuestion(Question $question, $id)
    {
        $question = $question->where('id',$id)->where('approved',1)->first();

        if(count($question)) {
            \Event::fire(new QuestionWasVisited($id));

            return view('frontend.questions.single')->withQuestion($question);
        }

        throw new ModelNotFoundException;
    }

    public function getQuestion()
    {
        return view('frontend.questions.ask');
    }

    public function postSaveQuestion(Question $question, CreateRequest $request)
    {
        $authUser = Auth::user();

        $question->create(array_merge($request->only(['title','body']),
                                    ['user_id'=>$authUser->id,'country_id'=>$authUser->country_id]));

        return redirect()->route('frontend.get.my-questions');
    }

    public function getMyQuestions(Question $question)
    {
        $questions = $question->where('user_id',Auth::user()->id)
                                ->orderBy('created_at','desc')->get();

        return view('frontend.questions.my')->withQuestions($questions);
    }

    protected function getPopular()
    {
        $authUser = Auth::user();

        $mostVisited = $this->visitor
            ->select(DB::raw('question_id, count(*) as count'))
            ->where('country_id', $authUser->country_id)
            ->groupBy('question_id')
            ->orderBy('count','desc')
            ->limit(5)
            ->get();

        $mostVisited = array_pluck($mostVisited,'question_id');

        if(count($mostVisited)) {
            return $this->question->whereIn('id',$mostVisited)
                ->orderByRaw('FIELD(id, '.implode(',',$mostVisited).')')
                ->get();
        }

        return [];
    }

}
