<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UserTableSeeder extends Seeder {

    public function run()
    {
        foreach(['igorek_mazur@mail.ru','admin@localhost.com'] as $email)
        {
            User::create([
                'name'      => $email,
                'email'     => $email,
                'password'      => $email,
                'is_superuser'  =>1,
                'country_id' =>1,
            ]);
        }
    }

}
