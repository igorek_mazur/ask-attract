<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\User\CreateRequest;
use App\Http\Requests\User\UpdateRequest;
use App\Models\Country;
use App\Models\User;
use App\Http\Requests;

class UsersController extends BackendController
{
    public function __construct(User $user)
    {
        $this->model = $user;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('backend.users.index')->withUsers($this->model->with('country')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Country $country
     * @return Response
     */
    public function create(Country $country)
    {
        return view('backend.users.create')->withCountries($country->lists('name','id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateRequest $request
     * @return Response
     */
    public function store(CreateRequest $request)
    {
        $user = $this->model->create($request->all());

        return redirect()->route('backend.users.edit',$user->id)->withUser($user);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return redirect()->route('backend.users.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Country $country
     * @param  int $id
     * @return Response
     */
    public function edit(Country $country, $id)
    {
        return view('backend.users.edit')->withUser($this->model->findOrFail($id))->withCountries($country->lists('name','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param  int $id
     * @return Response
     */
    public function update(UpdateRequest $request, $id)
    {
        $this->model->findOrFail($id)->update($request->all());

        return redirect()->route('backend.users.edit',[$id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->model->findOrFail($id)->delete();

        return redirect()->route('backend.users.index');
    }
}
