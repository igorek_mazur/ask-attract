@extends('app')

@section('top-scripts')

@stop

@section('content')
    <div class="col-xs-12">
        {!! Form::open(['route'=>'backend.countries.store','method'=>'post','class'=>'form-horizontal']) !!}
            <p class="caption-curd">Create New Country</p>
            <hr/>
            <div class="row">
                @include('partials.errors')
            </div>
            <div class="form-group">
                <div class="col-sm-4">
                    {!! Form::text('name',$value = null, ['class'=>'form-control','id'=>'inputName','placeholder'=>'Enter Country Name']) !!}
                </div>
                <div class="col-sm-6">
                    <button type="submit" class="btn btn-default">Save</button>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
@stop

@section('bottom-scripts')
z
@stop
