<?php

use App\Models\Country;
use Illuminate\Database\Seeder;

class CountryTableSeeder extends Seeder
{

	/**
	 *
	 */
	public function run()
	{
		foreach(['Ukraine','Afghanistan','Albania','Andorra','Angola',
		           'Argentina','Armenia','Australia','Bahamas',
					'Brazil','Canada','Chad','China','Colombia',
					'Costa Rica', 'Cuba',
		        ] as $name)
		{
			Country::create([
				'name'      => $name,
			]);
		}
	}

}
